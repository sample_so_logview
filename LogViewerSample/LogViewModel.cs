﻿using System;
using System.Collections.Generic;

namespace LogViewerSample
{
    internal class LogViewModel
    {
        public LogViewModel()
        {
            LogEntries = new List<LogEntry>
            {
                new LogEntry
                {
                    Criticalness = Criticalness.High,
                    EventId = 42,
                    Time = new DateTime(2010, 1, 1, 12, 0, 0, 0)
                },
                new LogEntry
                {
                    Criticalness = Criticalness.Internal,
                    EventId = 21,
                    Time = new DateTime(2010, 1, 1, 12, 0, 0, 0)
                },
                new LogEntry
                {
                    Criticalness = Criticalness.Normal,
                    EventId = 84,
                    Time = new DateTime(2010, 1, 1, 12, 0, 0, 0)
                }
            };
        }

        public List<LogEntry> LogEntries { get; private set; }
    }
}
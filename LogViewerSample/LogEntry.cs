﻿using System;

namespace LogViewerSample
{
    internal class LogEntry
    {
        public DateTime Time { get; set; }

        public int EventId { get; set; }

        public Criticalness Criticalness { get; set; }
    }

    public enum Criticalness
    {
        Internal,
        Normal,
        High
    }
}